# serve

Extend python's `http.server` to upload files

[![pipeline status](https://gitlab.com/yellowhat-labs/serve/badges/main/pipeline.svg)](https://gitlab.com/yellowhat-labs/serve/-/commits/main)
[![coverage report](https://gitlab.com/yellowhat-labs/serve/badges/main/coverage.svg)](https://gitlab.com/yellowhat-labs/serve/-/commits/main)

## Table of Contents

<!-- vim-markdown-toc GFM -->

* [Dependencies](#dependencies)
* [Installation](#installation)
* [Usage](#usage)

<!-- vim-markdown-toc -->

## Dependencies

* `python 3.12+`

## Installation

```bash
curl \
    --location \
    --remote-name \
    https://gitlab.com/yellowhat-labs/serve/-/raw/main/serve.py
```

## Usage

```bash
python serve.py --help
```
