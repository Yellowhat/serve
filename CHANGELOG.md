# [1.1.0](https://gitlab.com/yellowhat-labs/serve/compare/v1.0.0...v1.1.0) (2023-03-19)


### Features

* allow put requests ([0efaa87](https://gitlab.com/yellowhat-labs/serve/commit/0efaa87b9db6f3ef59bd547bc970da9b10865d55))

# [1.0.0](https://gitlab.com/yellowhat-labs/serve/compare/...v1.0.0) (2023-01-30)


### Bug Fixes

* initial release ([08d2225](https://gitlab.com/yellowhat-labs/serve/commit/08d22250742edb7cbdc670c0c4470bc3cef3bb91))
