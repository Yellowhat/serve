#!/usr/bin/env python
"""Start a server to download/upload files"""

import argparse
import http
import http.server
from os import chdir
from sys import maxsize
import socket
import socketserver
from email import message_from_bytes
from http import HTTPStatus
from pathlib import Path

__version__ = "1.1.0"


def auto_rename(path: Path) -> Path:
    """Rename a file if already present"""

    if not path.exists():
        return path
    for i in range(1, maxsize):
        renamed_path = Path(f"{path.stem}_{i}{path.suffix}")
        if not renamed_path.exists():
            return renamed_path
    raise FileExistsError(f"File {path} already exists")


class Handler(http.server.SimpleHTTPRequestHandler):
    """HTTP handler"""

    def do_GET(self) -> None:
        """GET Request"""
        if self.path == "/upload":
            self.send_upload_page()
        else:
            http.server.SimpleHTTPRequestHandler.do_GET(self)

    def do_POST(self) -> None:  # pylint: disable=invalid-name
        """POST Request"""
        if self.path == "/upload":
            result = self.receive_upload()
            if result[0] < HTTPStatus.BAD_REQUEST:
                self.send_response(result[0], result[1])
                self.end_headers()
            else:
                self.send_error(result[0], result[1])
        else:
            self.send_error(HTTPStatus.NOT_FOUND, "Can only POST/PUT to /upload")

    def do_PUT(self) -> None:  # pylint: disable=invalid-name
        """PUT Request"""
        self.do_POST()

    def send_upload_page(self) -> None:
        """Send upload page"""
        html = bytes(
            """
<!DOCTYPE html>
<html>
<head>
    <title>File Upload</title>
    <meta name="viewport" content="width=device-width, user-scalable=no"/>
</head>
<body>
    <h1>File Upload</h1>
    <form action="upload" method="POST" enctype="multipart/form-data">
        <input name="files" type="file" multiple/>
        <br/>
        <br/>
        <input type="submit"/>
    </form>
    <p id="task"></p>
    <p id="status"></p>
</body>
<script>
document.getElementsByTagName('form')[0].addEventListener('submit', e => {
    e.preventDefault()
    const formData = new FormData(e.target)
    const filenames = formData.getAll('files').map(v => v.name).join(', ')
    const request = new XMLHttpRequest()
    request.open(e.target.method, e.target.action)
    request.timeout = 3600000
    request.onreadystatechange = () => {
        if (request.readyState === XMLHttpRequest.DONE) {
            let message = `${request.status}: ${request.statusText}`
            if (request.status === 204) message = `Success. ${request.statusText}`
            if (request.status === 0) message = 'Connection failed'
            document.getElementById('status').textContent = message
        }
    }
    request.upload.onprogress = e => {
        let message = e.loaded === e.total ? 'Saving...' : `${Math.floor(100*e.loaded/e.total)}%`
        document.getElementById("status").textContent = message
    }
    request.send(formData)
    document.getElementById('task').textContent = `Uploading ${filenames}:`
    document.getElementById('status').textContent = '0%'
})
</script>
</html>""",
            "utf8",
        )
        self.send_response(HTTPStatus.OK)
        self.send_header("Content-Type", "text/html; charset=utf-8")
        self.send_header("Content-Length", str(len(html)))
        self.end_headers()
        self.wfile.write(html)

    def receive_upload(self) -> tuple[HTTPStatus, str]:
        """Receive upload"""
        result = (HTTPStatus.INTERNAL_SERVER_ERROR, "Server error")

        content_header = f"Content-Type: {self.headers.get('Content-Type')} \n\n"
        post_data = self.rfile.read(int(self.headers.get("Content-Length", 0)))
        msg = message_from_bytes(content_header.encode() + post_data)

        if not msg.is_multipart():
            return (
                HTTPStatus.BAD_REQUEST,
                "Payload is not a list of objects (multipart)",
            )

        for part in msg.get_payload():
            name = part.get_param("name", header="Content-Disposition")  # type: ignore[union-attr]
            if name != "files":
                return (HTTPStatus.BAD_REQUEST, "Field 'fields' not found")
            filename = part.get_param("filename", header="Content-Disposition")  # type: ignore[union-attr]
            if not filename:
                return (HTTPStatus.BAD_REQUEST, "No file selected")
            filepath = auto_rename(Path(filename))  # type: ignore[arg-type]
            with open(filepath.name, "wb") as obj:
                obj.write(part.get_payload(decode=True))  # type: ignore[arg-type,union-attr]
            self.log_message("Upload of '%s' accepted", filename)
            result = (HTTPStatus.NO_CONTENT, f"File saved as {filename}")

        return result


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=8080,
        nargs="?",
        help="Specify alternate port [default: 8080]",
    )
    parser.add_argument(
        "-d",
        "--directory",
        default=Path(".").absolute(),
        help="Specify alternative directory [default:current directory]",
    )
    args = parser.parse_args()

    if not (directory := Path(args.directory)).exists():
        directory = Path(".").absolute()
    chdir(directory)

    sokt = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sokt.connect(("10.255.255.255", 1))
    host = sokt.getsockname()[0]

    with socketserver.TCPServer(
        ("", args.port), Handler, bind_and_activate=False
    ) as server:
        server.allow_reuse_address = True
        server.server_bind()
        server.server_activate()
        print(f"[INFO] Port: {args.port}")
        print(f"[INFO] Path: {directory}")
        print("[INFO] File upload available at /upload")
        print(f"[INFO] http://{host}:{args.port}/")
        server.serve_forever()
