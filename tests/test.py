#!/usr/bin/env python
"""Tests"""

from pathlib import Path
from socket import create_connection
from sys import executable
from subprocess import DEVNULL, Popen, run  # nosec
from time import sleep
import pytest
import requests


class TestResource:
    """Test"""

    def __post_init__(self):
        self.server = None

    def spawn_server(self, port=None, directory=None) -> None:
        """Spawn server"""
        args = [executable, "serve.py"]
        if port is not None:
            args += ["-p", str(port)]
        if directory is not None:
            args += ["-d", directory]

        self.server = Popen(  # pylint: disable=consider-using-with # nosec B603
            args=args,
            shell=False,
        )
        # Wait for server to finish starting
        for _ in range(10):
            try:
                with create_connection(("127.0.0.1", port or 8080)):
                    break
            except ConnectionRefusedError:
                sleep(0.1)
        else:
            raise ValueError(f"Port {port or 8080} not responding")

    def get(self, path: str, port: int = 8080):
        """Request GET"""
        return requests.get(f"http://127.0.0.1:{port}{path}", timeout=10)

    def post(self, path: str, port: int = 8080, files=None):
        """Request POST"""
        return requests.post(f"http://127.0.0.1:{port}{path}", files=files, timeout=10)

    def put(self, path: str, port: int = 8080, files=None):
        """Request PUT"""
        return requests.put(f"http://127.0.0.1:{port}{path}", files=files, timeout=10)

    def teardown_method(self) -> None:
        """Teardown called for every method"""
        self.server.terminate()
        self.server.wait()

    def test_get(self) -> None:
        """GET"""
        self.spawn_server()

        res = self.get("/")
        assert res.status_code == 200  # nosec B101

    def test_get_port(self) -> None:
        """GET - port"""
        self.spawn_server(port=8020)

        res = self.get("/", port=8020)
        assert res.status_code == 200  # nosec B101
        with pytest.raises(requests.ConnectionError):
            self.get("/")

    def test_post(self) -> None:
        """GET - /upload"""
        self.spawn_server()

        res = self.get("/upload")
        assert res.status_code == 200  # nosec B101

    def test_post_file(self) -> None:
        """POST - file"""
        self.spawn_server()

        res = self.post("/upload", files={"files": ("file-0", "content")})
        assert res.status_code == 204  # nosec B101

        with open("file-0", encoding="utf8") as obj:
            assert obj.read() == "content"  # nosec B101

    def test_post_same_name(self) -> None:
        """POST - same name overwrite"""
        self.spawn_server()

        res = self.post("/upload", files={"files": ("same", "content")})
        assert res.status_code == 204  # nosec B101
        res = self.post("/upload", files={"files": ("same", "content-new")})
        assert res.status_code == 204  # nosec B101

        with open("same", encoding="utf8") as obj:
            assert obj.read() == "content"  # nosec B101
        with open("same_1", encoding="utf8") as obj:
            assert obj.read() == "content-new"  # nosec B101

    def test_url_encoded_file_name(self) -> None:
        """POST - encoded name"""
        self.spawn_server()

        res = self.post("/upload", files={"files": ("url%2Eencoding.txt", "content")})
        assert res.status_code == 204  # nosec B101

        with open("url%2Eencoding.txt", encoding="utf8") as obj:
            assert obj.read() == "content"  # nosec B101

    def test_post_no_files(self) -> None:
        """POST - No file defined"""
        self.spawn_server()

        res = self.post("/upload", files={"files": ("", "")})
        assert res.status_code == 400  # nosec B101

    def test_post_bad_path(self) -> None:
        """POST - bad path"""
        self.spawn_server()

        res = self.post("/uploadx", files={"file_foo": ("bad", "content")})
        assert res.status_code == 404  # nosec B101

    def test_post_bad_field(self) -> None:
        """POST - bad field"""
        self.spawn_server()

        res = self.post("/upload", files={"file_foo": ("a", "content")})
        assert res.status_code == 400  # nosec B101

    def test_post_multiple_files(self) -> None:
        """POST - multiple files"""
        self.spawn_server()

        res = self.post(
            "/upload",
            files=[
                ("files", ("file-1", "file-content-1")),
                ("files", ("file-2", "file-content-2")),
            ],
        )
        assert res.status_code == 204  # nosec B101

        with open("file-1", encoding="utf8") as obj:
            assert obj.read() == "file-content-1"  # nosec B101
        with open("file-2", encoding="utf8") as obj:
            assert obj.read() == "file-content-2"  # nosec B101

    def test_post_directory(self) -> None:
        """POST - directory"""
        self.spawn_server(directory="/tmp")  # nosec B108

        res = self.post(
            "/upload",
            files={
                "files": ("tmp-tmp", "tmp-content"),
            },
        )
        assert res.status_code == 204  # nosec B101

        with open("/tmp/tmp-tmp", encoding="utf8") as obj:  # nosec B108
            assert obj.read() == "tmp-content"  # nosec B101

    def test_post_directory_traversal(self) -> None:
        """POST - directory traversal"""
        self.spawn_server()

        res = self.post(
            "/upload",
            files={
                "files": ("/tmp/tmp", "tmp-content"),  # nosec B108
            },
        )
        assert res.status_code == 204  # nosec B101

        with open("tmp", encoding="utf8") as obj:
            assert obj.read() == "tmp-content"  # nosec B101
        assert not Path("/tmp/tmp").exists()  # nosec B101 B108

    def test_post_curl_single(self) -> None:
        """POST - curl single file"""
        self.spawn_server()

        args = [
            "curl",
            "--request",
            "POST",
            "http://localhost:8080/upload",
            "--form",
            "files=@/etc/hosts",
        ]
        res = run(args=args, check=True, stdout=DEVNULL, stderr=DEVNULL)  # nosec B603
        assert res.returncode == 0  # nosec B101

        with (
            open("hosts", encoding="utf8") as obj_new,
            open("/etc/hosts", encoding="utf8") as obj_ref,
        ):
            assert obj_new.read() == obj_ref.read()  # nosec B101

    def test_post_curl_multiple(self) -> None:
        """POST - curl multiple files"""
        self.spawn_server()

        args = [
            "curl",
            "--request",
            "POST",
            "http://localhost:8080/upload",
            "--form",
            "files=@/etc/group",
            "--form",
            "files=@/etc/hosts",
        ]
        res = run(args=args, check=True, stdout=DEVNULL, stderr=DEVNULL)  # nosec B603
        assert res.returncode == 0  # nosec B101

        with (
            open("group", encoding="utf8") as obj_new,
            open("/etc/group", encoding="utf8") as obj_ref,
        ):
            assert obj_new.read() == obj_ref.read()  # nosec B101
        with (
            open("hosts", encoding="utf8") as obj_new,
            open("/etc/hosts", encoding="utf8") as obj_ref,
        ):
            assert obj_new.read() == obj_ref.read()  # nosec B101

    def test_put_file(self) -> None:
        """PUT - file"""
        self.spawn_server()

        res = self.put("/upload", files={"files": ("file-put", "content")})
        assert res.status_code == 204  # nosec B101

        with open("file-put", encoding="utf8") as obj:
            assert obj.read() == "content"  # nosec B101
